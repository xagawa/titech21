# pip3 install PyCryptodome
# or pip install PyCryptodome
# See https://pycryptodome.readthedocs.io/en/latest/index.html
from Crypto.PublicKey import ECC
from Crypto.Util.number import bytes_to_long, long_to_bytes
# pip3 install ecdsa
# or pip install ecdsa
# See https://pypi.org/project/ecdsa/
from ecdsa import ellipticcurve, VerifyingKey
from ecdsa.ecdsa import curve_384, generator_384, Public_key, Private_key
# Default
from hashlib import sha1, sha256
from random import randint
import json

# We use 'secp384r1'

def my_ecdsa_sign(msg, privkey):
    hsh = sha256(msg).digest()
    nonce = sha1(msg).digest()
    sig = privkey.sign(bytes_to_long(hsh), bytes_to_long(nonce))
    return {"r": hex(sig.r), "s": hex(sig.s)}

if __name__=="__main__":

	# Read ecdsa keys 
	# See https://pycryptodome.readthedocs.io/en/latest/src/public_key/ecc.html
	with open('3d-ecdsa-priv.pem','rt') as f:
		key = ECC.import_key(f.read())


	print(key.curve)
	print(f"Q.x:{key.pointQ.x}")
	print(f"Q.y:{key.pointQ.y}")


	G = generator_384
	n = G.order()
	Qp= (key.d)*G

	print(f"G:{G}")
	print(f"G.x:{G.x()}")
	print(f"G.y:{G.y()}")
	print(f"Qp.x:{Qp.x()}")
	print(f"Qp.y:{Qp.y()}")

	print(f"n:{n}")

	pubkey = Public_key(G, Qp)
	privkey = Private_key(pubkey, int(key.d))

	# Sign on shattered-1.pdf
	with open("shattered-1.pdf",'rb') as file1:
		msg1 = file1.read()

	hsh = sha1(msg1).digest()
	print(1,hsh.hex())

	sig1 = my_ecdsa_sign(msg1, privkey)

	with open("ecdsa-sig1",'w') as cert1:
		cert1.write(str(sig1))

	# Sign on shattered-2.pdf
	with open("shattered-2.pdf",'rb') as file2:
		msg2 = file2.read()
		
	hsh = sha1(msg2).digest()
	print(2,hsh.hex())

	sig2 = my_ecdsa_sign(msg2, privkey)

	with open("ecdsa-sig2",'w') as cert2:
		cert2.write(str(sig2))

	"""
	# Utility
	with open("shattered-1.pdf",'rb') as file1:
		msg1 = file1.read()
		z1 = bytes_to_long(sha256(msg1).digest())

	with open("ecdsa-sig1",'r') as sig:
		sig1 = sig.readline()
		sig1_JSON = json.loads(sig1)
		r1 = int(sig1_JSON['r'],16)
		s1 = int(sig1_JSON['s'],16)
	"""
