from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

if __name__=="__main__":

	pt0 = b'\x00' * 16
	iv0 = b'\x00' * 16
	n = 2**16
	s = 0

	for i in range(n):
		# we use AES-CFB8
		# See https://www.pycryptodome.org/en/latest/src/cipher/classic.html#cfb-mode
		key = get_random_bytes(16)
		cipher = AES.new(key,AES.MODE_CFB,iv=iv0,segment_size=8)
		ct = cipher.encrypt(pt0)
		if ct == b'\x00'*16: 
			s += 1
		print(f"{i:5}", key.hex(), ct.hex())

	print(f"# of all-0 cts / # of tests = {s}/{n}")

