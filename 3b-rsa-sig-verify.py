# See https://pycryptodome.readthedocs.io/en/latest/index.html
# pip install pycryptodome 
# or pip3 install pycryptodome

from Crypto.PublicKey import RSA

# parse h as 0001 | ffff (...) (at least 4 octets) | 00 | m 
def padding_check(h):
	hs, h = h[0:4], h[4:]
	if hs != "0001":
	        return False, "1"
	hs, h = h[0:8], h[8:]
	if hs != "ffffffff":
		return False, "2"
	while True:
		hs, h = h[0:2], h[2:]
		if hs == "00":
			break
		elif hs != "ff":
			return False, "3"
	# take 20 octets
	m = h[0:40]
	return True, m

def Verify(n,e,m,s):

	h = pow(int(s,16),e,n)
	h = f"{h:0256x}" # left padding by 0

	parse_flag, mp = padding_check(h) 
	return parse_flag and (mp == m)


def find_approx_cubicroot(c):
	# you need to write a program...
	return c

if __name__=="__main__":
	# Read RSA keys
	# See https://pycryptodome.readthedocs.io/en/latest/src/public_key/rsa.html
	with open("3b-rsa-pub.pem",'rb') as key:
		pubkey = RSA.importKey(key.read())
	n = pubkey.n
	e = pubkey.e # will be 3
	print(f"n: {n:#x}")
	print(f"e: {e:#x}")

	with open("3b.txt",'r') as m_and_s:
		# strip *=0x and \n
		# keep m and s as string
		m = m_and_s.readline()[4:-1]
		s = m_and_s.readline()[4:-1]
	print(len(m),len(s))
	print(f"m: {m}")
	print(f"s: {s}")
	print(Verify(n,e,m,s))



